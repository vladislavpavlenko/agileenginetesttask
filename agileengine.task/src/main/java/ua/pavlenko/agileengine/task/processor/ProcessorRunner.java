package ua.pavlenko.agileengine.task.processor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@PropertySource("classpath:api.properties")
public class ProcessorRunner {

    @Value("${api.update.initial.delay.minutes}")
    private Long initialDelay;

    @Value("${api.update.period.minutes}")
    private Long updatePeriod;

    @Autowired
    private CacheImagesProcessor cacheImagesProcessor;

    public void runProcessors() {
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(cacheImagesProcessor, initialDelay, updatePeriod, TimeUnit.MINUTES);
    }
}
