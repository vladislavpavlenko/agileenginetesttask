package ua.pavlenko.agileengine.task;

import ua.pavlenko.agileengine.task.processor.ProcessorRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
		ProcessorRunner processorRunner = (ProcessorRunner) context.getBean("processorRunner");
		processorRunner.runProcessors();
	}
}
