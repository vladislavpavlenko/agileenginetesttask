package ua.pavlenko.agileengine.task.repository.impl;

import ua.pavlenko.agileengine.task.model.Image;
import ua.pavlenko.agileengine.task.repository.ImagesRepository;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class ImagesRepositoryImpl implements ImagesRepository {

    private Map<String, Image> imageMap;

    @PostConstruct
    public void init() {
        imageMap = new HashMap<>();
    }

    public void addImage(Image image) {
        imageMap.put(image.getId(), image);
    }

    public Collection<Image> findAll() {
        return imageMap.values();
    }
}
