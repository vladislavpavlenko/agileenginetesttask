package ua.pavlenko.agileengine.task.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Image {

    @JsonProperty("id")
    private String id;

    @JsonProperty("author")
    private String author;

    @JsonProperty("camera")
    private String cameraName;

    @JsonProperty("cropped_picture")
    private String croppedPicturePath;

    @JsonProperty("full_picture")
    private String fullImagePath;

    @JsonProperty("tags")
    private String tags;

    @Override
    public String toString() {
        return ((id != null ? id : " ") +
                (author != null ? author : " ") +
                (cameraName != null ? cameraName : " ") +
                (tags != null ? tags : " ")).trim();
    }
}
