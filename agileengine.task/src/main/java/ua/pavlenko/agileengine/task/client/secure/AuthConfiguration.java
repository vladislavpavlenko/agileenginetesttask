package ua.pavlenko.agileengine.task.client.secure;

import ch.qos.logback.classic.Logger;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import ua.pavlenko.agileengine.task.client.secure.dto.ApiTokenRequest;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@Configuration
@PropertySource("classpath:api.properties")
public class AuthConfiguration {

    private static final Logger LOG = (Logger) LoggerFactory.getLogger(AuthConfiguration.class);

    @Value("${api.key}")
    @Setter
    @Getter
    private String apiKeyValue;

    @Bean
    public HttpEntity<String> getAuthHttpEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        ApiTokenRequest apiTokenRequest = new ApiTokenRequest();
        apiTokenRequest.setApiKey(apiKeyValue);

        String json = null;
        try {
            json = new ObjectMapper().writeValueAsString(apiTokenRequest);
        } catch (JsonProcessingException e) {
            LOG.error(e.getMessage(), e);
        }
        return new HttpEntity<>(json, headers);
    }
}
