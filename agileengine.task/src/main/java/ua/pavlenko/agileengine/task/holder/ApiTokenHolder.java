package ua.pavlenko.agileengine.task.holder;

import lombok.Getter;
import lombok.Setter;
import ua.pavlenko.agileengine.task.client.secure.AuthClient;
import ua.pavlenko.agileengine.task.client.secure.dto.ApiTokenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ApiTokenHolder {

    @Getter
    @Setter
    private ApiTokenResponse apiTokenResponse;

    @Autowired
    private AuthClient authClient;

    @PostConstruct
    public void init() {
        apiTokenResponse = authClient.getApiToken();
    }

    public HttpEntity<String> buildAuthHttpEntity() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Bearer " + apiTokenResponse.getToken());
        return new HttpEntity<>(httpHeaders);
    }
}
