package ua.pavlenko.agileengine.task.client.secure.dto;

import lombok.Data;

@Data
public class ApiTokenResponse {

    private String token;
}
