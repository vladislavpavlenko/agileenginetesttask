package ua.pavlenko.agileengine.task.client.secure;

import ua.pavlenko.agileengine.task.client.secure.dto.ApiTokenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@PropertySource("classpath:api.properties")
public class AuthClient {

    @Autowired
    private HttpEntity<String> authRequest;

    @Value("${api.uri.auth}")
    private String authUri;

    public ApiTokenResponse getApiToken() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<ApiTokenResponse> apiTokenResponseResponseEntity
                = restTemplate.postForEntity(authUri, authRequest, ApiTokenResponse.class);
        return apiTokenResponseResponseEntity.getBody();
    }
}
