package ua.pavlenko.agileengine.task.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import ua.pavlenko.agileengine.task.model.Image;

import java.util.List;

@Data
@AllArgsConstructor
public class ImageSearchResponseDto {

    private List<Image> images;

    private boolean hasCoincidences;

    private Long countCoincidences;

    private String resultString;
}
