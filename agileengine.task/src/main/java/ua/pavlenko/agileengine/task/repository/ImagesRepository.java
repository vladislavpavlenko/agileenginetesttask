package ua.pavlenko.agileengine.task.repository;

import ua.pavlenko.agileengine.task.model.Image;

import java.util.Collection;

public interface ImagesRepository {

    void addImage(Image image);

    Collection<Image> findAll();
}
