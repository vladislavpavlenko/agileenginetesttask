package ua.pavlenko.agileengine.task.service;

import ua.pavlenko.agileengine.task.dto.ImageSearchResponseDto;

public interface ImagesService {

    ImageSearchResponseDto getImagesBySearchTerm(String searchTerm);
}
