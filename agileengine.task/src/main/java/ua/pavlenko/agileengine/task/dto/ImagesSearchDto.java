package ua.pavlenko.agileengine.task.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ImagesSearchDto {

    @JsonProperty("pictures")
    private List<ImageDto> images;

    @JsonProperty("page")
    private Long page;

    @JsonProperty("pageCount")
    private Long pageCount;

    @JsonProperty("hasMore")
    private boolean hasMore;
}
