package ua.pavlenko.agileengine.task.client.image;

import ua.pavlenko.agileengine.task.dto.ImagesSearchDto;
import ua.pavlenko.agileengine.task.holder.ApiTokenHolder;
import ua.pavlenko.agileengine.task.model.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@PropertySource("classpath:api.properties")
public class ImagesClient {

    @Value("${api.uri.images}")
    private String imagesPath;

    @Autowired
    private ApiTokenHolder apiTokenHolder;

    public ImagesSearchDto downloadImagesByPageNumber(Long pageNumber) {
        RestTemplate restTemplate = new RestTemplate();

        UriComponentsBuilder uriComponentsBuilder
                = UriComponentsBuilder.fromUriString(imagesPath)
                .queryParam("page", pageNumber);

        ResponseEntity<ImagesSearchDto> imagesSearchDtoResponseEntity
                = restTemplate.exchange(uriComponentsBuilder.toUriString(), HttpMethod.GET,
                apiTokenHolder.buildAuthHttpEntity(), ImagesSearchDto.class);
        return imagesSearchDtoResponseEntity.getBody();
    }

    public Image downloadFullImageInfoById(String imageId) {
        RestTemplate restTemplate = new RestTemplate();

        String imagePath = UriComponentsBuilder.fromUriString(imagesPath)
                .path("/{id}").build(imageId).toString();

        ResponseEntity<Image> imageResponseEntity
                = restTemplate.exchange(imagePath, HttpMethod.GET,
                apiTokenHolder.buildAuthHttpEntity(), Image.class);

        return imageResponseEntity.getBody();
    }
}
