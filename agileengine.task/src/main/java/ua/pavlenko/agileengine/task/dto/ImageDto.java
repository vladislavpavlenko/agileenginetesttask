package ua.pavlenko.agileengine.task.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ImageDto {

    @JsonProperty("id")
    private String id;

    @JsonProperty("cropped_picture")
    private String croppedPicturePath;
}
