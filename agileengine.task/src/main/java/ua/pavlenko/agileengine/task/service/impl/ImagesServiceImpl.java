package ua.pavlenko.agileengine.task.service.impl;

import ua.pavlenko.agileengine.task.dto.ImageSearchResponseDto;
import ua.pavlenko.agileengine.task.model.Image;
import ua.pavlenko.agileengine.task.repository.ImagesRepository;
import ua.pavlenko.agileengine.task.service.ImagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ImagesServiceImpl implements ImagesService {

    @Autowired
    private ImagesRepository imagesRepository;

    @Override
    public ImageSearchResponseDto getImagesBySearchTerm(String searchTerm) {
        ImageSearchResponseDto imageSearchResponseDto
                = new ImageSearchResponseDto(new ArrayList<>(), false,
                0L, "fail");
        for (Image image : imagesRepository.findAll()) {
            if (searchForCoincidences(image, searchTerm)) {
                imageSearchResponseDto.getImages().add(image);
                imageSearchResponseDto.setHasCoincidences(true);
                imageSearchResponseDto.setCountCoincidences(imageSearchResponseDto.getCountCoincidences() + 1);
            }
        }
        return imageSearchResponseDto;
    }

    private boolean searchForCoincidences(Image image, String searchTerm) {
        return image.toString().toLowerCase().contains(searchTerm.toLowerCase().trim());
    }
}
