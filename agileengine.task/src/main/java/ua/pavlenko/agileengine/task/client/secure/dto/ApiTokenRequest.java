package ua.pavlenko.agileengine.task.client.secure.dto;

import lombok.Data;

@Data
public class ApiTokenRequest {

    private String apiKey;
}
