package ua.pavlenko.agileengine.task.processor;

import ch.qos.logback.classic.Logger;
import ua.pavlenko.agileengine.task.client.image.ImagesClient;
import ua.pavlenko.agileengine.task.dto.ImageDto;
import ua.pavlenko.agileengine.task.dto.ImagesSearchDto;
import ua.pavlenko.agileengine.task.model.Image;
import ua.pavlenko.agileengine.task.repository.ImagesRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CacheImagesProcessor implements Runnable {

    private static final Logger LOG = (Logger) LoggerFactory.getLogger(CacheImagesProcessor.class);

    @Autowired
    private ImagesRepository imagesRepository;

    @Autowired
    private ImagesClient imagesClient;

    public void cacheImages() {
        LOG.info("Start image processing");
        Long currentPage = 1L;
        Long pageCount;
        boolean stopFlag = false;
        while (!stopFlag) {
            ImagesSearchDto imagesSearchDto = imagesClient.downloadImagesByPageNumber(currentPage);
            createOrUpdateImages(imagesSearchDto.getImages());
            pageCount = imagesSearchDto.getPageCount();
            if (currentPage >= pageCount) {
                stopFlag = true;
            } else {
                currentPage++;
            }
        }
        LOG.info("Finished image processing");
    }

    private void createOrUpdateImages(List<ImageDto> imageDtos) {
        for (ImageDto imageDto : imageDtos) {
            Image image = imagesClient.downloadFullImageInfoById(imageDto.getId());
            imagesRepository.addImage(image);
        }
    }

    @Override
    public void run() {
        cacheImages();
    }
}
