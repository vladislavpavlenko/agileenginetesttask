package ua.pavlenko.agileengine.task.controller;

import ua.pavlenko.agileengine.task.dto.ImageSearchResponseDto;
import ua.pavlenko.agileengine.task.service.ImagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ImagesController {

    @Autowired
    private ImagesService imagesService;

    @GetMapping("/search/{searchTerm}")
    public ModelAndView searchPictureByTag(@PathVariable(name = "searchTerm") String searchTerm, ModelAndView modelAndView) {
        ImageSearchResponseDto imageSearchResponseDto = imagesService.getImagesBySearchTerm(searchTerm);
        if (imageSearchResponseDto.isHasCoincidences()) {
            imageSearchResponseDto.setResultString("success");
        }
        modelAndView.setViewName("search");
        modelAndView.addObject(imageSearchResponseDto);
        return modelAndView;
    }

}
