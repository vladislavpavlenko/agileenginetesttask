# AgileEngineTestTask

The application is developed using Java 11 and Spring Boot 2.3

Start-up instruction:
1. Clone this repository;
2. Open the application folder using IDEA;
3. Download maven dependencies;
4. Run the application.


Instruction of use:
1. Follow this link: http://localhost:8087/search/{your search criteria};
2. To configure the application open the file src\main\resources\api.properties:
    api.key - key to receive a token;
    api.uri.auth - path to get a token by key;
    api.uri.images - path for receiving pictures;
    api.update.initial.delay.minutes - initial delay time for cache picture engine;
    api.update.period.minutes - period time for cache picture engine.
